package main.java.com.basetis.mail;

import java.util.*;
import javax.mail.*;
import main.java.com.basetis.parse.ParseHtml;

public class ReadingEmail {
    
	private Session session;
	private String protocol;
	private String urlImap;
	private String account; 
	private String pwd;
	
    public ReadingEmail(String protocol, String urlImap, String account, String pwd) {
		
    	Properties props;
		this.protocol = protocol;
		this.urlImap = urlImap;
		this.account = account;
		this.pwd = pwd;
		
		try {
			props = new Properties();
	        props.setProperty(this.protocol, "imaps");
	        this.session = Session.getInstance(props, null);
	        
        } catch (Exception mex) {
            mex.printStackTrace();
        }
		
	}

    public void getMessages () {
        try {
        	Store store;
        	Folder inbox;
        	store = this.session.getStore();
	        store.connect(this.urlImap, this.account, this.pwd);
	        inbox = store.getFolder("INBOX");
	        inbox.open(Folder.READ_ONLY);
	        
        	// Get messages
 			Message messages[] = inbox.getMessages();
 			
 			int vCount = 0;
 			int vCountGlobal = 0;
 			int vNumTotal = messages.length;
 			
 			System.out.println("Reading " + vNumTotal + " messages...");
 			// Display the messages
 			for(Message message:messages) {
 				for (Address a: message.getFrom())
 				{
 					//System.out.println("Date: " + message.getSentDate() + " - From:" + a.toString());
 					
 					if ((a.toString()).contains("ofertas@push.infojobs.net") ){
 						System.out.println("Correo " + (vCount + 1) + " - " + vCountGlobal + " de " + vNumTotal);
 						//System.out.println("Date: " + message.getSentDate() + " - From:" + a.toString());
 						System.out.println("Date: " + message.getSentDate() + " - From:" + a.toString());
 	 					System.out.println("Title: " + message.getSubject());
		 				System.out.println();
		 				//System.out.println(message.getContent());
		 				
		 				ParseHtml mailEmpleos = new ParseHtml();
		 				mailEmpleos.parseHtmlString((String)message.getContent());
		 				mailEmpleos.printOfertas(); 
		 				
		 				System.out.println("---------------");
		 				vCount++;
 					}	 	
 					vCountGlobal++;
 				}
 				if (vCount > 0)
 					break;
 			}
 			
 			//close the store and folder objects
 			inbox.close(false);
 		    store.close();
 		    
            /*
            Message msg = inbox.getMessage(inbox.getMessageCount());
            Address[] in = msg.getFrom();
            for (Address address : in) {
                System.out.println("FROM:" + address.toString());
            }
            Multipart mp = (Multipart) msg.getContent();
            BodyPart bp = mp.getBodyPart(0);
            System.out.println("SENT DATE:" + msg.getSentDate());
            System.out.println("SUBJECT:" + msg.getSubject());
            System.out.println("CONTENT:" + bp.getContent());
            */
            
        } catch (Exception mex) {
            mex.printStackTrace();
        }
    }

    public List<String> getFromEmailAddresses () {
		return null;    	
    }
    
    
}