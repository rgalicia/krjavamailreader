package main.java.com.basetis.persistence;

import java.util.Date;

public class TbKrMailInfo {
    private Long idMailInfo;
    private Date dateMail;
    private String fromMail;
    private String titleMail;
    private String empresaOfeta;
    private Date dateOferta;
    private String tituloOferta;
    private String estudiosOferta;
    private String expOferta;
    private String conocOferta;
    private String descOferta;
    private String tipoIndOferta;
    private String categOferta;
    private String nivelOferta;
    private String vacantesOferta;
    private String localOferta;
    private String salarioOferta;
    private String jornadaOferta;
    private String idiomaOferta;
    private Date inicioOferta;
    private String contratoOferta;
    private String contentOferta;
    

    public TbKrMailInfo() {}

    public Long getIdMailInfo() {
        return idMailInfo;
    }

    public void setIdMailInfo(Long idMailInfo) {
        this.idMailInfo = idMailInfo;
    }

	public Date getDateMail() {
		return dateMail;
	}

	public void setDateMail(Date dateMail) {
		this.dateMail = dateMail;
	}

	public String getFromMail() {
		return fromMail;
	}

	public void setFromMail(String fromMail) {
		this.fromMail = fromMail;
	}

	public String getTitleMail() {
		return titleMail;
	}

	public void setTitleMail(String titleMail) {
		this.titleMail = titleMail;
	}

	public String getEmpresaOfeta() {
		return empresaOfeta;
	}

	public void setEmpresaOfeta(String empresaOfeta) {
		this.empresaOfeta = empresaOfeta;
	}

	public Date getDateOferta() {
		return dateOferta;
	}

	public void setDateOferta(Date dateOferta) {
		this.dateOferta = dateOferta;
	}

	public String getTituloOferta() {
		return tituloOferta;
	}

	public void setTituloOferta(String tituloOferta) {
		this.tituloOferta = tituloOferta;
	}

	public String getEstudiosOferta() {
		return estudiosOferta;
	}

	public void setEstudiosOferta(String estudiosOferta) {
		this.estudiosOferta = estudiosOferta;
	}

	public String getExpOferta() {
		return expOferta;
	}

	public void setExpOferta(String expOferta) {
		this.expOferta = expOferta;
	}

	public String getConocOferta() {
		return conocOferta;
	}

	public void setConocOferta(String conocOferta) {
		this.conocOferta = conocOferta;
	}

	public String getDescOferta() {
		return descOferta;
	}

	public void setDescOferta(String descOferta) {
		this.descOferta = descOferta;
	}

	public String getTipoIndOferta() {
		return tipoIndOferta;
	}

	public void setTipoIndOferta(String tipoIndOferta) {
		this.tipoIndOferta = tipoIndOferta;
	}

	public String getCategOferta() {
		return categOferta;
	}

	public void setCategOferta(String categOferta) {
		this.categOferta = categOferta;
	}

	public String getNivelOferta() {
		return nivelOferta;
	}

	public void setNivelOferta(String nivelOferta) {
		this.nivelOferta = nivelOferta;
	}

	public String getVacantesOferta() {
		return vacantesOferta;
	}

	public void setVacantesOferta(String vacantesOferta) {
		this.vacantesOferta = vacantesOferta;
	}

	public String getLocalOferta() {
		return localOferta;
	}

	public void setLocalOferta(String localOferta) {
		this.localOferta = localOferta;
	}

	public String getSalarioOferta() {
		return salarioOferta;
	}

	public void setSalarioOferta(String salarioOferta) {
		this.salarioOferta = salarioOferta;
	}

	public String getJornadaOferta() {
		return jornadaOferta;
	}

	public void setJornadaOferta(String jornadaOferta) {
		this.jornadaOferta = jornadaOferta;
	}

	public String getIdiomaOferta() {
		return idiomaOferta;
	}

	public void setIdiomaOferta(String idiomaOferta) {
		this.idiomaOferta = idiomaOferta;
	}

	public Date getInicioOferta() {
		return inicioOferta;
	}

	public void setInicioOferta(Date inicioOferta) {
		this.inicioOferta = inicioOferta;
	}

	public String getContratoOferta() {
		return contratoOferta;
	}

	public void setContratoOferta(String contratoOferta) {
		this.contratoOferta = contratoOferta;
	}

	public String getContentOferta() {
		return contentOferta;
	}

	public void setContentOferta(String contentOferta) {
		this.contentOferta = contentOferta;
	}
    
}