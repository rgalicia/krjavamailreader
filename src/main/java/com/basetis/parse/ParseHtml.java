package main.java.com.basetis.parse;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ParseHtml {
    
	private Document doc;
	
    public ParseHtml() {
		this.doc = null;
	}

    public void parseHtmlString (String pHtml) {
    	this.doc = Jsoup.parse(pHtml);
    }

    public void parseHtmlURL (String pURL) throws IOException {
    	this.doc = Jsoup.connect(pURL).get();
    }
    
    public void printOfertas () {
    	Elements links = this.doc.select("a[id^=oferta_]"); // empieza por oferta_
    	    	
    	for (Element link : links) {
		  System.out.println("Id: " + link.attr("id") + " - URL: " + link.attr("href"));
		}
    }
	
	
    
    
    public String getHtmlHref () {
    	
	    Element link = this.doc.select("a").first();
	    String relHref = link.attr("href"); // == "/"
	    String absHref = link.attr("abs:href"); // "http://jsoup.org/"
		
		/******/
		
		Elements links = doc.select("a[href]"); // a with href
		
		
		Elements pngs = doc.select("img[src$=.png]");
		  // img with src ending .png
		Element masthead = doc.select("div.masthead").first();
		  // div with class=masthead
		Elements resultLinks = doc.select("h3.r > a"); // direct a after h3
		
		/*******/
		
		Element content = doc.getElementById("content");
		Elements links1 = content.getElementsByTag("a");
		for (Element link1 : links1) {
		  String linkHref = link.attr("href");
		  String linkText = link.text();
		}
		
		/*******/
		  
		  String html = "<p>An <a href='http://example.com/'><b>example</b></a> link.</p>";
		  Document doc = Jsoup.parse(html);
		  Element link2 = doc.select("a").first();

		  String text = doc.body().text(); // "An example link"
		  String linkHref = link2.attr("href"); // "http://example.com/"
		  String linkText = link2.text(); // "example""

		  String linkOuterH = link2.outerHtml(); 
		      // "<a href="http://example.com"><b>example</b></a>"
		  String linkInnerH = link2.html(); // "<b>example</b>"
		  
		  return absHref;
		}
		
}